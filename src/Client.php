<?php

namespace Tinaba\Pay;


use Tinaba\Pay\Actions\TinabaAction;
use Tinaba\Pay\Base\Action;
use Tinaba\Pay\Objects\CheckoutStateCallback;
use Tinaba\Pay\Objects\ConfirmPreauthorizedCheckoutRequest;
use Tinaba\Pay\Objects\ConfirmPreauthorizedCheckoutResponse;
use Tinaba\Pay\Objects\GetCheckoutListRequest;
use Tinaba\Pay\Objects\GetCheckoutListResponse;
use Tinaba\Pay\Objects\InitCheckoutRequest;
use Tinaba\Pay\Objects\InitCheckoutResponse;
use Tinaba\Pay\Objects\RefundCheckoutRequest;
use Tinaba\Pay\Objects\RefundCheckoutResponse;
use Tinaba\Pay\Objects\VerifyCheckoutRequest;
use Tinaba\Pay\Objects\VerifyCheckoutResponse;

class Client
{

    /**
     * @var ApiContext
     */
    protected $context;

    /**
     * @var ActionFactory
     */
    protected $factory;

    /**
     * @var TinabaAction
     */
    protected $currentAction;

    public function __construct(ApiContext $context)
    {

        $this->context = $context;
        $this->factory = new ActionFactory($this->context);

    }

    /**
     * Make the requested action
     *
     * @param string $action
     *
     * @return TinabaAction
     */
    protected function makeAction($action)
    {
        /**
         * @var TinabaAction $action
         */
        $action = $this->factory->make($action);
        $this->currentAction = $action;
        return $action;
    }

    /**
     * Return the latest instantiated action
     *
     * @return TinabaAction
     */
    public function getAction()
    {
        return $this->currentAction;
    }


    /**
     * Initialize a new checkout
     *
     * @param InitCheckoutRequest $request
     * @return InitCheckoutResponse
     * @throws
     */
    public function initCheckout(InitCheckoutRequest $request)
    {

        $action = $this->makeAction('initCheckout');

        $action->setRequestParameters($request);

        return $action->send();

    }

    /**
     * Confirm a preatuhorized checkout
     *
     * @param ConfirmPreauthorizedCheckoutRequest $request
     * @return ConfirmPreauthorizedCheckoutResponse
     * @throws
     */
    public function confirmPreauthorizedCheckout(ConfirmPreauthorizedCheckoutRequest $request)
    {

        $action = $this->makeAction('confirmPreauthorizedCheckout');
        $action->setRequestParameters($request);

        return $action->send();
    }

    /**
     * Verify the checkout state
     *
     * @param VerifyCheckoutRequest $request
     * @return VerifyCheckoutResponse
     * @throws
     */
    public function verifyCheckout(VerifyCheckoutRequest $request)
    {

        $action = $this->makeAction('verifyCheckout');
        $action->setRequestParameters($request);

        return $action->send();

    }

    /**
     * Refund a checkout
     *
     * @param RefundCheckoutRequest $request
     * @return RefundCheckoutResponse
     * @throws
     */
    public function refundCheckout(RefundCheckoutRequest $request)
    {

        $action = $this->makeAction('refundCheckout');
        $action->setRequestParameters($request);

        return $action->send();

    }

    /**
     * Get the list of the merchant checkouts
     *
     * @param GetCheckoutListRequest $request
     * @return GetCheckoutListResponse
     * @throws
     */
    public function getCheckoutList(GetCheckoutListRequest $request)
    {

        $action = $this->makeAction('getCheckoutList');
        $action->setRequestParameters($request);

        return $action->send();

    }

    /**
     * Verifies the validity of the signature returned by the callback
     *
     * @param CheckoutStateCallback $callback
     * @return bool
     */
    public function verifyCallback(CheckoutStateCallback $callback)
    {

        $secret = $this->context->getCredentials()->getSecret();

        $plainText = $callback->externalId.$callback->checkoutState.$secret;
        $hash = base64_encode(hash('sha256', $plainText, true));

        return hash_equals($hash, $callback->signature);

    }

}
