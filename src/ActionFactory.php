<?php
/**
 * Created by PhpStorm.
 * User: desh
 * Date: 15/03/18
 * Time: 15.40
 */

namespace Tinaba\Pay;


use Tinaba\Pay\Base\Factory;

class ActionFactory extends Factory
{

    protected $actionsNamespace = 'Tinaba\Pay\Actions';

    protected $actions = [
        'confirmPreauthorizedCheckout' => 'ConfirmPreauthorizedCheckout',
        'getCheckoutList' => 'GetCheckoutList',
        'initCheckout' => 'InitCheckout',
        'refundCheckout' => 'RefundCheckout',
        'verifyCheckout' => 'VerifyCheckout'
    ];
}