<?php

namespace Tinaba\Pay;

use Tinaba\Pay\Base\ApiContext as BaseApiContext;
use Tinaba\Pay\Base\Credentials;

class ApiContext extends BaseApiContext
{

    protected $productionUrl = 'https://tfull.bancaprofilo.it/WebApi/tinaba';

    protected $sandboxUrl = 'https://valid.tinaba.tv/WebApi/tinaba';

    protected $defaultConfig = [
        'sandbox' => false,
        'merchantId' => '',
        'secret' => '',
        'timeout' => 20
    ];

    public function __construct(array $customConfig)
    {

        $config = array_merge($this->defaultConfig, $customConfig);

        $this->baseUrl = isset($config['sandbox']) && $config['sandbox'] === true ?
            $this->sandboxUrl : $this->productionUrl;

        parent::__construct($config);
    }

    /**
     * @param array $config
     * @return Credentials
     */
    protected function buildCredentials(array $config)
    {

        return new TinabaCredentials($config['merchantId'], $config['secret']);

    }

    /**
     * @return TinabaCredentials
     */
    public function getCredentials()
    {
        /**
         * @var TinabaCredentials $credentials
         */
        $credentials = parent::getCredentials();

        return $credentials;
    }

}