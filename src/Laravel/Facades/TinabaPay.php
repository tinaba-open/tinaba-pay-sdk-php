<?php

namespace Tinaba\Pay\Laravel\Facades;


use Illuminate\Support\Facades\Facade;

class TinabaPay extends Facade
{

    protected static function getFacadeAccessor()
    {
        return 'tinaba.pay';
    }

}