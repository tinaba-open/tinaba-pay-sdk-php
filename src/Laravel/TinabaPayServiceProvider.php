<?php
/**
 * Created by PhpStorm.
 * User: desh
 * Date: 15/03/18
 * Time: 16.05
 */

namespace Tinaba\Pay\Laravel;


use Illuminate\Support\ServiceProvider;
use Tinaba\Pay\ApiContext;
use Tinaba\Pay\Client;

class TinabaPayServiceProvider extends ServiceProvider
{

    public function boot()
    {

        $this->publishes([
            __DIR__.'/config/tinaba-pay.php' => config_path('tinaba-pay.php')
        ], 'config');

    }

    public function register()
    {

        $this->mergeConfigFrom(__DIR__. '/config/tinaba-pay.php', 'tinaba-pay');

        $this->app->bind('tinaba.pay', function($app) {

            $context = new ApiContext($app['config']['tinaba-pay']);
            $context->setValidationFactory($app['validator']);
            return new Client($context);

        });

    }

}