<?php


return [

    'sandbox' => env('TINABA_PAY_SANDBOX_MODE', false),
    'merchantId' => env('TINABA_PAY_MERCHANT_ID', ''),
    'secret' => env('TINABA_PAY_SECRET', ''),
    'timeout' => env('TINABA_REQUEST_TIMEOUT', 20)

];