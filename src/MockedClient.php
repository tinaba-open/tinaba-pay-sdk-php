<?php

namespace Tinaba\Pay;


class MockedClient extends Client
{

    /**
     * Mocked status code
     *
     * @var int
     */
    protected $mockedStatusCode = 200;

    /**
     * Mocked headers
     *
     * @var array
     */
    protected $mockedHeaders = [];

    /**
     * Mocked response body
     *
     * @var array
     */
    protected $mockedBody = [];

    /**
     * Returns an instance of a mocked action that returns a predefined response
     *
     * @param string $action
     * @return \Mockery\Mock|Actions\TinabaAction
     * @throws \Exception
     */
    public function makeAction($action)
    {
        $action = parent::makeAction($action);

        $mockedAction = $action->mockResponse($this->mockedStatusCode, $this->mockedBody, $this->mockedHeaders);

        $this->currentAction = $mockedAction;

        return $mockedAction;
    }

    /**
     * Sets the mocked status code
     *
     * @param integer $statusCode
     * @return self
     */
    public function setMockedStatusCode($statusCode)
    {

        $this->mockedStatusCode = $statusCode;

        return $this;

    }

    /**
     * Sets the mocked headers
     *
     * @param array $headers
     * @return $this
     */
    public function setMockedHeaders($headers)
    {
        $this->mockedHeaders = $headers;

        return $this;
    }

    /**
     * Sets the mocked body
     *
     * @param array $body
     * @return $this
     */
    public function setMockedBody($body)
    {
        $this->mockedBody = $body;

        return $this;
    }

}