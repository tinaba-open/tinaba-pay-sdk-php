<?php

namespace Tinaba\Pay\Actions;

use Tinaba\Pay\Objects\VerifyCheckoutRequest;
use Tinaba\Pay\Objects\VerifyCheckoutResponse;

class VerifyCheckout extends TinabaAction
{

    protected $requestName = 'verifyCheckoutRequest';

    protected $responseName = 'verifyCheckoutResponse';

    protected $endpoint = 'checkout/verifyCheckout';

    protected $method = 'POST';

    protected function getRequestParamsRules()
    {
        return VerifyCheckoutRequest::getValidationRules();
    }

    protected function getSignatureBody()
    {
        $merchantId = $this->context->getCredentials()->getMerchantId();
        $externalId = $this->getRequestParameter('externalId');
        $secret = $this->context->getCredentials()->getSecret();

        return $merchantId.$externalId.$secret;
    }

    protected function parseResponse($response)
    {
        return VerifyCheckoutResponse::parse($response);
    }

}