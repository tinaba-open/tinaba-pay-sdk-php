<?php

namespace Tinaba\Pay\Actions;


use GuzzleHttp\Psr7;
use GuzzleHttp\Psr7\Response as GuzzleResponse;
use Tinaba\Pay\Base\Action;
use Tinaba\Pay\ApiContext;
use GuzzleHttp\Psr7\Request as Psr7Request;
use Tinaba\Pay\Base\ApiObject;
use Tinaba\Pay\Base\Request;

abstract class TinabaAction extends Action
{

    /**
     * @var ApiContext
     */
    protected $context;

    /**
     * @var string
     */
    protected $requestName;

    /**
     * @var string
     */
    protected $responseName;

    /**
     * @var string
     */
    protected $endpoint;

    /**
     * @var string
     */
    protected $method = 'GET';

    /**
     * TinabaAction constructor.
     * @param ApiContext $context
     */
    public function __construct(ApiContext $context)
    {
        parent::__construct($context);
    }

    protected function getEndpoint()
    {
        return $this->endpoint;
    }

    protected function getMethod()
    {
        return $this->method;
    }

    protected function getHeaders()
    {
        return [];
    }

    protected function validationRules() {

        return [];

    }

    protected function getResponseParamsRules()
    {
        return array_merge([

            'merchantId' => 'required|string',
            'signature' => 'required|string'

        ], $this->validationRules());
    }

    protected function getUrlParametersRules()
    {
        return [];
    }

    protected function getPayloadType()
    {
        return 'json';
    }

    protected function getRequestName()
    {
        return $this->requestName;
    }

    protected function getResponseName()
    {
        return $this->responseName;
    }

    /**
     * @param ApiObject $requestObject
     * @return void
     */
    public function setRequestParameters(ApiObject $requestObject)
    {

        $this->request_params = $requestObject->toArray(true);

    }


    protected function authenticate(Psr7Request $request)
    {

        $requestName = $this->getRequestName();
        $merchantId = $this->context->getCredentials()->getMerchantId();

        $plainText = $this->getSignatureBody();
        $signature = hash('sha256', $plainText, true);

        $body = json_decode($request->getBody()->getContents(), true);
        $body['data']['request'][$requestName]['signature'] = base64_encode($signature);
        $body['data']['request'][$requestName]['merchantId'] = $merchantId;

        $bodyStream = Psr7\stream_for(json_encode($body));
        return $request->withBody($bodyStream);

    }

    /**
     * Mocks the response
     *
     * @param int $statusCode
     * @param array $body
     * @param array $headers
     *
     * @return \Mockery\Mock
     * @throws \Exception
     */
    public function mockResponse($statusCode, $body = [], $headers = [])
    {

        $mockedAction = \Mockery::mock(static::class, [
            $this->context
        ])->makePartial();

        $mockedAction->request_params = $this->request_params;
        $mockedAction->url_params = $this->url_params;

        $baseHeaders = [
            'Content-Type' => 'application/json'
        ];

        $mockedResponse = new GuzzleResponse($statusCode, array_merge($headers, $baseHeaders), json_encode($body));

        $mockedRequest = Request::mock(
            $mockedResponse,
            $this->context->getBaseUrl(),
            $this->context->getTimeout(),
            $this->getPayloadType(),
            $this->getMethod(),
            $this->buildUrl(),
            $this->getHeaders(),
            $this->buildPayload(),
            $this->getAuthenticateCallable(),
            $this->getHttpAuthCredentials()
        );

        $mockedAction->shouldAllowMockingProtectedMethods()
            ->shouldReceive('makeRequest')
            ->andReturn($mockedRequest);

        return $mockedAction;

    }
}