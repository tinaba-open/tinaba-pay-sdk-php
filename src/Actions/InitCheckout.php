<?php

namespace Tinaba\Pay\Actions;

use Tinaba\Pay\Objects\InitCheckoutRequest;
use Tinaba\Pay\Objects\InitCheckoutResponse;

class InitCheckout extends TinabaAction
{

    protected $requestName = 'initCheckoutRequest';

    protected $responseName = 'initCheckoutResponse';

    protected $endpoint = 'checkout/initCheckout';

    protected $method = 'POST';

    protected function getRequestParamsRules()
    {
        return InitCheckoutRequest::getValidationRules();
    }

    protected function getSignatureBody()
    {
        $merchantId = $this->context->getCredentials()->getMerchantId();
        $externalId = $this->getRequestParameter('externalId');
        $amount = $this->getRequestParameter('amount');
        $currency = $this->getRequestParameter('currency');
        $creationDate = $this->getRequestParameter('creationDate');
        $creationTime = $this->getRequestParameter('creationTime');
        $secret = $this->context->getCredentials()->getSecret();

        return $merchantId.$externalId.$amount.$currency.$creationDate.$creationTime.$secret;
    }

    protected function parseResponse($response)
    {
        return InitCheckoutResponse::parse($response);
    }

}