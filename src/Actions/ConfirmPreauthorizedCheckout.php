<?php

namespace Tinaba\Pay\Actions;

use GuzzleHttp\Psr7;
use GuzzleHttp\Psr7\Request as Psr7Request;
use Tinaba\Pay\Exceptions\TinabaError;
use Tinaba\Pay\Objects\ConfirmPreauthorizedCheckoutRequest;
use Tinaba\Pay\Objects\ConfirmPreauthorizedCheckoutResponse;

class ConfirmPreauthorizedCheckout extends TinabaAction
{

    protected $requestName = 'confirmPreauthorizedCheckoutRequest';

    protected $responseName = 'confirmP2PtransferByCaptureResponse';

    protected $endpoint = 'pagamento/confirmP2PtransferByCapture';

    protected $method = 'POST';

    protected function getRequestParamsRules()
    {
        return ConfirmPreauthorizedCheckoutRequest::getValidationRules();
    }

    /**
     *
     * @param array $payload
     * @throws
     */
    public function checkForErrors(array $payload)
    {

        $payload = $payload['confirmPreauthorizedCheckoutResponse'];
        if(isset($payload) && $payload['status'] !== '000')
        {

            throw TinabaError::parse($payload['status'], $payload['errorCode']);

        }

    }

    protected function getSignatureBody()
    {
        $merchantId = $this->context->getCredentials()->getMerchantId();
        $externalId = $this->getRequestParameter('externalId');
        $amount = $this->getRequestParameter('amount');
        $secret = $this->context->getCredentials()->getSecret();

        return $merchantId.$externalId.$amount.$secret;
    }

    /**
     * Builds the request payload
     *
     * @return array
     */
    protected function buildPayload()
    {
        return [
            'data' => [
                'request' => [
                    'confirmP2PtransferByCaptureRequest' => [
                        $this->getRequestName() => $this->request_params
                    ]
                ]
            ]
        ];
    }

    protected function parseResponse($response)
    {
        return ConfirmPreauthorizedCheckoutResponse::parse($response['confirmPreauthorizedCheckoutResponse']);
    }

    protected function authenticate(Psr7Request $request)
    {

        $merchantId = $this->context->getCredentials()->getMerchantId();

        $plainText = $this->getSignatureBody();
        $signature = hash('sha256', $plainText, true);

        $body = json_decode($request->getBody()->getContents(), true);
        $body['data']['request']['confirmP2PtransferByCaptureRequest'][$this->requestName]['signature'] = base64_encode($signature);
        $body['data']['request']['confirmP2PtransferByCaptureRequest'][$this->requestName]['merchantId'] = $merchantId;

        $bodyStream = Psr7\stream_for(json_encode($body));
        return $request->withBody($bodyStream);

    }

}