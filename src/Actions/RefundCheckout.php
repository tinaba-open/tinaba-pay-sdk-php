<?php

namespace Tinaba\Pay\Actions;

use Tinaba\Pay\Objects\RefundCheckoutResponse;
use Tinaba\Pay\Objects\RefundCheckoutRequest;

class RefundCheckout extends TinabaAction
{

    protected $requestName = 'refundCheckoutRequest';

    protected $responseName = 'refundCheckoutResponse';

    protected $endpoint = 'checkout/refundCheckout';

    protected $method = 'POST';

    protected function getRequestParamsRules()
    {
        return RefundCheckoutRequest::getValidationRules();
    }

    protected function getSignatureBody()
    {
        $merchantId = $this->context->getCredentials()->getMerchantId();
        $externalId = $this->getRequestParameter('externalId');
        $amount = $this->getRequestParameter('amount');
        $secret = $this->context->getCredentials()->getSecret();

        return $merchantId.$externalId.$amount.$secret;
    }

    protected function parseResponse($response)
    {
        return RefundCheckoutResponse::parse($response);
    }

}