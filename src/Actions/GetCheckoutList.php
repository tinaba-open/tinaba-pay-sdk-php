<?php

namespace Tinaba\Pay\Actions;

use Tinaba\Pay\Objects\GetCheckoutListRequest;
use Tinaba\Pay\Objects\GetCheckoutListResponse;

class GetCheckoutList extends TinabaAction
{

    protected $requestName = 'getCheckoutListRequest';

    protected $responseName = 'getCheckoutListResponse';

    protected $endpoint = 'checkout/getCheckoutList';

    protected $method = 'POST';

    protected function getRequestParamsRules()
    {
        return GetCheckoutListRequest::getValidationRules();
    }



    protected function getSignatureBody()
    {
        $merchantId = $this->context->getCredentials()->getMerchantId();
        $secret = $this->context->getCredentials()->getSecret();

        return $merchantId.$secret;
    }

    protected function parseResponse($response)
    {
        return GetCheckoutListResponse::parse($response);
    }

}