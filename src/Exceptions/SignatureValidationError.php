<?php

namespace Tinaba\Pay\Exceptions;


use Throwable;

class SignatureValidationError extends TinabaError
{

    public function __construct($status, $errorCode, Throwable $previous = null)
    {
        parent::__construct($status, $errorCode, self::SIGNATURE_VALIDATION_ERROR, $previous);
    }

}