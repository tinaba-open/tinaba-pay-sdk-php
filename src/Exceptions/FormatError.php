<?php

namespace Tinaba\Pay\Exceptions;


use Throwable;

class FormatError extends TinabaError
{

    public function __construct($status, $errorCode, Throwable $previous = null)
    {
        parent::__construct($status, $errorCode, self::FORMAT_ERROR, $previous);
    }

}