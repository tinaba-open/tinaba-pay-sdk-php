<?php namespace Tinaba\Pay\Exceptions;

use Illuminate\Validation\Validator;

class UrlParametersException extends BaseException
{

    /**
     * @var Validator
     */
    protected $validator;

    protected $message = "Request failed due to validation error on the url parameters";

    /**
     * APIArgumentsException constructor.
     * @param Validator $validator
     */
    public function __construct(Validator $validator)
    {
        $this->validator = $validator;
        parent::__construct($this->message . ": " . json_encode($this->getValidationError()), BaseException::VALIDATION_ERROR);
    }

    /**
     * @return Validator
     */
    public function getValidator()
    {
        return $this->validator;
    }

    /**
     * @return array
     */
    public function getValidationError()
    {
        return $this->validator->errors()->all();
    }
}