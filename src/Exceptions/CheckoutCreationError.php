<?php

namespace Tinaba\Pay\Exceptions;


use Throwable;

class CheckoutCreationError extends TinabaError
{

    public function __construct($status, $errorCode, Throwable $previous = null)
    {
        parent::__construct($status, $errorCode, self::CHECKOUT_CREATION_ERROR, $previous);
    }

}