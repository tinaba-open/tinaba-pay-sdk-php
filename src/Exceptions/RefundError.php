<?php

namespace Tinaba\Pay\Exceptions;


use Throwable;

class RefundError extends TinabaError
{

    public function __construct($status, $errorCode, Throwable $previous = null)
    {
        parent::__construct($status, $errorCode, self::REFUND_ERROR, $previous);
    }

}