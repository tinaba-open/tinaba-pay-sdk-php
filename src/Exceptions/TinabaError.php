<?php

namespace Tinaba\Pay\Exceptions;


use Throwable;

class TinabaError extends \Exception
{

    const UNKNOWN_ERROR = 0x001;
    const FORMAT_ERROR = 0x002;
    const MERCHANT_CONFIG_ERROR = 0x003;
    const CHECKOUT_CREATION_ERROR = 0x004;
    const PAYMENT_VALIDATION_ERROR = 0x005;
    const SIGNATURE_VALIDATION_ERROR = 0x006;
    const REFUND_ERROR = 0x007;

    const ERROR_CODES_MAP = [

        'IGE' => UnknownError::class,
        'EPE' => FormatError::class,
        'MCE' => MerchantConfigError::class,
        'CKE' => CheckoutCreationError::class,
        'PYE' => PaymentValidationError::class,
        'SGE' => SignatureValidationError::class,
        'RFE' => RefundError::class

    ];

    protected $status;
    protected $errorCode;

    public function __construct($status, $errorCode, $code, Throwable $previous = null)
    {

        $this->status = $status;
        $this->errorCode = $errorCode;
        $message = "Request failed with status '$status' and errorCode '$errorCode'";
        parent::__construct($message, $code, $previous);

    }

    public static function parse($status, $errorCode)
    {

        $errorPrefix = substr(strtoupper($errorCode), 0, 3);

        if(array_key_exists($errorPrefix, self::ERROR_CODES_MAP)) {

            $exceptionClass = self::ERROR_CODES_MAP[$errorPrefix];
            return new $exceptionClass($status, $errorCode);

        }

        return new UnknownError($status, $errorCode);
    }

    public function getErrorCode()
    {
        return $this->errorCode;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function toArray()
    {

        return [
            'code' => $this->getCode(),
            'status' => $this->status,
            'errorCode' => $this->errorCode
        ];

    }

}