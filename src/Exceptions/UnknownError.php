<?php

namespace Tinaba\Pay\Exceptions;


use Throwable;

class UnknownError extends TinabaError
{

    public function __construct($status, $errorCode, Throwable $previous = null)
    {
        parent::__construct($status, $errorCode, self::UNKNOWN_ERROR, $previous);
    }

}