<?php

namespace Tinaba\Pay\Exceptions;


use Throwable;

class MerchantConfigError extends TinabaError
{

    public function __construct($status, $errorCode, Throwable $previous = null)
    {

        parent::__construct($status, $errorCode, self::MERCHANT_CONFIG_ERROR, $previous);
    }

}