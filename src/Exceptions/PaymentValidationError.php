<?php

namespace Tinaba\Pay\Exceptions;


use Throwable;

class PaymentValidationError extends TinabaError
{

    public function __construct($status, $errorCode, Throwable $previous = null)
    {

        parent::__construct($status, $errorCode, self::PAYMENT_VALIDATION_ERROR, $previous);
    }

}