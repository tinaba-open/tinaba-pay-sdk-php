<?php
/**
 * Created by PhpStorm.
 * User: desh
 * Date: 15/03/18
 * Time: 11.34
 */

namespace Tinaba\Pay\Objects;


use Tinaba\Pay\Base\ApiObject;

/**
 * Class VerifyCheckoutRequest
 * @package Tinaba\Pay\Objects
 *
 * @property string $externalId
 */
class VerifyCheckoutRequest extends ApiObject
{

    protected $attributes = [
        'externalId' => null,
    ];

    public static function getValidationRules($parent = '')
    {

        return [
            'externalId' => 'required|string',
        ];

    }

    /**
     * Set the external id
     *
     * @param string $externalId
     * @return self
     */
    public function setExternalId($externalId)
    {
        $this->externalId = $externalId;
        return $this;
    }
}