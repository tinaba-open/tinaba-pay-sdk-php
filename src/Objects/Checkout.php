<?php

namespace Tinaba\Pay\Objects;

use Carbon\Carbon;
use Tinaba\Pay\Base\ApiObject;

/**
 * Class Checkout
 * @package Tinaba\Pay\Objects
 *
 * @property string $externalId
 * @property string $merchantId
 * @property string $amount
 * @property string $currency
 * @property Carbon $authTime
 * @property string $transactionType
 * @property string $state
 *
 */
class Checkout extends ApiObject
{

    protected $attributes = [
        'merchantId' => null,
        'externalId' => null,
        'amount' => null,
        'currency' => null,
        'authTime' => null,
        'transactionType' => null,
        'state' => null,
        'internalTransactionId' => null
    ];

    protected $dates = [
        'authTime'
    ];


    /**
     * Returns the checkout authorization time as a carbon object
     *
     * @return null|Carbon
     */
    public function getAuthTimeAttribute()
    {
        if($this->attributes['authTime']) {
            return Carbon::createFromFormat('Y-m-d-H.i.s', $this->attributes['authTime']);
        }

        return null;
    }
}