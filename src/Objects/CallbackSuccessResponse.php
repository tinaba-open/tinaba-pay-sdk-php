<?php

namespace Tinaba\Pay\Objects;

use Carbon\Carbon;
use Tinaba\Pay\Base\ApiObject;

/**
 * Class CheckoutStateCallback
 * @package Tinaba\Pay\Objects
 *
 * @property string $status
 *
 */
class CallbackSuccessResponse extends ApiObject
{

    protected $attributes = [
        'status' => '000',
    ];

}