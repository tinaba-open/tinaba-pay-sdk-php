<?php

namespace Tinaba\Pay\Objects;

use Carbon\Carbon;
use Tinaba\Pay\Base\ApiObject;

/**
 * Class UserAddress
 * @package Tinaba\Pay\Objects
 *
 * @property string $receiverName
 * @property string $address
 * @property string $streetNumber
 * @property string $city
 * @property string $cap
 * @property string $district
 * @property string $country
 * @property string $sendAt
 * @property string $phoneNumber
 *
 */
class ShippingAddress extends ApiObject
{

    protected $attributes = [
        'receiverName' => null,
        'address' => null,
        'streetNumber' => null,
        'city' => null,
        'cap' => null,
        'district' => null,
        'country' => null,
        'sendAt' => null,
        'phoneNumber' => null
    ];

}