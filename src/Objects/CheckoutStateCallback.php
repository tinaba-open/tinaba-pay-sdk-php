<?php

namespace Tinaba\Pay\Objects;

use Carbon\Carbon;
use Tinaba\Pay\Base\ApiObject;

/**
 * Class CheckoutStateCallback
 * @package Tinaba\Pay\Objects
 *
 * @property string $externalId
 * @property string $checkoutState
 * @property string $signature
 * @property UserAddress $userAddress
 *
 */
class CheckoutStateCallback extends ApiObject
{

    protected $attributes = [
        'externalId' => null,
        'checkoutState' => null,
        'signature' => null,
        'userAddress' => null
    ];

    public static function getRelations()
    {
        return [
            'userAddress' => UserAddress::class
        ];
    }

}