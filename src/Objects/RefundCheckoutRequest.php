<?php
/**
 * Created by PhpStorm.
 * User: desh
 * Date: 15/03/18
 * Time: 11.34
 */

namespace Tinaba\Pay\Objects;


use Tinaba\Pay\Base\ApiObject;

/**
 * Class RefundCheckoutRequest
 * @package Tinaba\Pay\Objects
 *
 * @property string $externalId
 * @property string $amount
 */
class RefundCheckoutRequest extends ApiObject
{

    protected $attributes = [
        'externalId' => null,
        'amount' => null
    ];

    public static function getValidationRules($parent = '')
    {

        return [
            'externalId' => 'required|string',
            'amount' => 'required|string'
        ];

    }

    /**
     * Set the external id
     *
     * @param string $externalId
     * @return self
     */
    public function setExternalId($externalId)
    {
        $this->externalId = $externalId;
        return $this;
    }

    /**
     * Set the amount
     *
     * @param string $amount
     * @return self
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
        return $this;
    }
}