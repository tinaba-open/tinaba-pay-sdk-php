<?php

namespace Tinaba\Pay\Objects;

use Carbon\Carbon;
use Tinaba\Pay\Base\ApiObject;

/**
 * Class UserAddress
 * @package Tinaba\Pay\Objects
 *
 * @property string $name
 * @property string $surname
 * @property string $email
 * @property ShippingAddress $shippingAddress
 * @property BillingAddress $billingAddress
 *
 */
class UserAddress extends ApiObject
{

    protected $attributes = [
        'name' => null,
        'surname' => null,
        'email' => null,
        'shippingAddress' => null,
        'billingAddress' => null,
    ];

    public static function getRelations()
    {
        return [
            'shippingAddress' => ShippingAddress::class,
            'billingAddress' => BillingAddress::class
        ];
    }

}