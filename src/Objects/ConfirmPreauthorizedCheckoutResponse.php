<?php

namespace Tinaba\Pay\Objects;

/**
 * Class ConfirmPreauthorizedCheckoutResponse
 * @package Tinaba\Pay\Objects
 *
 * @property string $externalId
 * @property string $amount
 */
class ConfirmPreauthorizedCheckoutResponse extends TinabaResponse
{

}