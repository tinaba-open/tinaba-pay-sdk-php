<?php
/**
 * Created by PhpStorm.
 * User: desh
 * Date: 15/03/18
 * Time: 11.34
 */

namespace Tinaba\Pay\Objects;

/**
 * Class InitCheckoutResponse
 * @package Tinaba\Pay\Objects
 *
 * @property string $paymentCode
 * @property string $code
 */
class InitCheckoutResponse extends TinabaResponse
{

    protected $attributes = [
        'status' => null,
        'paymentCode' => null,
        'errorCode' => null
    ];

    /**
     * Returns the payment code
     * @return string
     */
    public function getCodeAttribute()
    {
        return $this->paymentCode;
    }



}