<?php

namespace Tinaba\Pay\Objects;

/**
 * Class RefundCheckoutResponse
 * @package Tinaba\Pay\Objects
 *
 * @property string $externalId
 */
class RefundCheckoutResponse extends TinabaResponse
{

}