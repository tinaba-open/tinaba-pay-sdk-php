<?php

namespace Tinaba\Pay\Objects;

/**
 * Class VerifyCheckoutResponse
 * @package Tinaba\Pay\Objects
 *
 * @property string $externalId
 * @property string $checkoutState
 * @property string $amount
 * @property string $currency
 * @property UserAddress $userAddress
 *
 */
class VerifyCheckoutResponse extends TinabaResponse
{

    const CHECKOUT_STATUS_COMPLETED = '000';
    const CHECKOUT_STATUS_PAYMENT_FAILED = '001';
    const CHECKOUT_STATUS_EXPIRED = '002';
    const CHECKOUT_STATUS_PENDING = '003';
    const CHECKOUT_STATUS_ALREADY_PAYED = '004';
    const CHECKOUT_STATUS_PREAUTHORIZED = '005';

    protected $attributes = [
        'status' => null,
        'checkoutState' => null,
        'externalId' => null,
        'amount' => null,
        'currency' => null,
        'errorCode' => null,
        'userAddress' => null
    ];

    public static function getRelations()
    {

        return [
            'userAddress' => UserAddress::class
        ];

    }

}