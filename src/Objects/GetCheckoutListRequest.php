<?php
/**
 * Created by PhpStorm.
 * User: desh
 * Date: 15/03/18
 * Time: 11.34
 */

namespace Tinaba\Pay\Objects;


use function GuzzleHttp\Psr7\str;
use Tinaba\Pay\Base\ApiObject;

/**
 * Class GetCheckoutListRequest
 * @package Tinaba\Pay\Objects
 *
 * @property string $dateFrom
 * @property string $dateTo
 */
class GetCheckoutListRequest extends ApiObject
{

    protected $attributes = [
        'dateFrom' => null,
        'dateTo' => null
    ];

    public static function getValidationRules($parent = '')
    {

        return [
            'dateFrom' => 'required|string|date_format:Y-m-d',
            'dateTo' => 'required|string|date_format:Y-m-d'
        ];

    }

    /**
     * Set the dateFrom parameter
     *
     * @param string $year
     * @param string $month
     * @param string $day
     * @return self
     */
    public function setDateFrom($year, $month, $day)
    {
        $month = str_pad($month, 2, '0', STR_PAD_LEFT);
        $day = str_pad($day, 2, '0', STR_PAD_LEFT);
        $this->dateFrom = "$year-$month-$day";
        return $this;
    }

    /**
     * Set the dateFrom parameter
     *
     * @param string $year
     * @param string $month
     * @param string $day
     * @return self
     */
    public function setDateTo($year, $month, $day)
    {
        $month = str_pad($month, 2, '0', STR_PAD_LEFT);
        $day = str_pad($day, 2, '0', STR_PAD_LEFT);
        $this->dateTo = "$year-$month-$day";
        return $this;
    }
}