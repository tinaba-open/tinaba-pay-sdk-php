<?php

namespace Tinaba\Pay\Objects;

/**
 * Class GetCheckoutListResponse
 * @package Tinaba\Pay\Objects
 *
 * @property string $externalId
 * @property array $checkoutList
 */
class GetCheckoutListResponse extends TinabaResponse
{

    protected $attributes = [
        'status' => null,
        'errorCode' => null,
        'checkoutList' => null
    ];

    public static function getRelations()
    {
        return [
            'checkoutList' => 'array|' . Checkout::class
        ];
    }

}