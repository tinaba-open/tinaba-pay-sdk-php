<?php

namespace Tinaba\Pay\Objects;


use Carbon\Carbon;
use Tinaba\Pay\Base\ApiObject;

/**
 * Class InitCheckoutRequest
 * @package Tinaba\Pay\Objects
 *
 * @property string $externalId
 * @property string $amount
 * @property string $currency
 * @property string $description
 * @property string $validTo
 * @property string $creationDate
 * @property string $creationTime
 * @property string $paymentMode
 * @property string $productCodeId
 * @property string $metadata
 * @property string $notificationCallback
 * @property string $notificationHttpMethod
 * @property string $backCallback
 * @property string $successCallback
 * @property string $failureCallback
 * @property string $creationDateTime
 * @property bool $sendReceiverAddress
 */
class InitCheckoutRequest extends ApiObject
{

    const MODE_ECOMMERCE = 'ECOMMERCE';
    const MODE_MEDIA = 'MEDIA';
    const MODE_PREAUTH = 'PREAUTH';

    protected $attributes = [
        'externalId' => null,
        'amount' => null,
        'currency' => null,
        'description' => null,
        'validTo' => null,
        'creationDate' => null,
        'creationTime' => null,
        'paymentMode' => null,
        'productCodeId' => null,
        'metadata' => null,
        'notificationCallback' => null,
        'notificationHttpMethod' => null,
        'backCallback' => null,
        'successCallback' => null,
        'failureCallback' => null,
        'sendReceiverAddress' => null
    ];

    public static function getValidationRules($parent = '')
    {
        return [
            'externalId' => 'required|string',
            'amount' => 'required|string',
            'currency' => 'required|string',
            'description' => 'sometimes|string',
            'validTo' => 'sometimes|string',
            'creationDate' => 'required|string|date_format:Ymd',
            'creationTime' => 'required|string|date_format:His',
            'paymentMode' => 'required|string|in:ECOMMERCE,MEDIA,PREAUTH',
            'productCodeId' => 'required_if:paymentMode,MEDIA|string',
            'metadata' => 'sometimes|string',
            'notificationCallback' => 'required|string',
            'notificationHttpMethod' => 'required|string|in:GET,POST,PUT,PATCH,get,post,put,patch',
            'backCallback' => 'sometimes|string',
            'successCallback' => 'sometimes|string',
            'failureCallback' => 'sometimes|string',
            'sendReceiverAddress' => 'sometimes|boolean'
        ];
    }

    /**
     * Set the external id
     *
     * @param string $externalId
     * @return self
     */
    public function setExternalId($externalId)
    {
        $this->externalId = $externalId;
        return $this;
    }

    /**
     * Set the amount value
     *
     * @param string $amount
     * @return self
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
        return $this;
    }

    /**
     * Set the currency
     *
     * @param string $currency
     * @return self
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;
        return $this;
    }

    /**
     * Set the description
     *
     * @param string $description
     * @return self
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * Set checkout expiration in minutes
     *
     * @param string $validTo
     * @return self
     */
    public function setValidTo($validTo)
    {
        $this->validTo = $validTo;
        return $this;
    }

    /**
     * Set the creation datetime of the checkout
     *
     * @param Carbon $creationDateTime
     * @return self
     */
    public function setCreationDateTime(Carbon $creationDateTime)
    {

        return $this->setCreationDate($creationDateTime->format('Ymd'))
                ->setCreationTime($creationDateTime->format('His'));

    }

    /**
     * Get the creation datetime as a carbon object
     *
     * @return Carbon
     */
    public function getCreationDateTimeAttribute()
    {
        return Carbon::createFromFormat('YmdHis', $this->creationDate.$this->creationTime);
    }

    /**
     * Set the creation date of the checkout
     *
     * @param string $creationDate
     * @return self
     */
    protected function setCreationDate($creationDate)
    {
        $this->creationDate = $creationDate;
        return $this;
    }

    /**
     * Set the creation time of the checkout
     *
     * @param string $creationTime
     * @return self
     */
    protected function setCreationTime($creationTime)
    {
        $this->creationTime = $creationTime;
        return $this;
    }

    /**
     * Set the checkout payment mode, possible values are ECOMMERCE, MEDIA, PREAUTH
     *
     * @param string $paymentMode
     * @return self
     */
    public function setPaymentMode($paymentMode)
    {
        $this->paymentMode = $paymentMode;
        return $this;
    }

    /**
     * Set the product code id, required if paymentMode is set to MEDIA
     *
     * @param $productCodeId
     * @return self
     */
    public function setProductCodeId($productCodeId)
    {
        $this->productCodeId = $productCodeId;
        return $this;
    }

    /**
     * Set the checkout metadata
     *
     * @param string $metadata
     * @return self
     */
    public function setMetadata($metadata)
    {
        $this->metadata = $metadata;
        return $this;
    }

    /**
     * Set the notification callback url
     *
     * @param string $notificationUrl
     * @return self
     */
    public function setNotificationUrl($notificationUrl)
    {
        $this->notificationCallback = $notificationUrl;
        return $this;
    }

    /**
     * Set the notification callback HTTP method
     *
     * @param string $method
     * @return self
     */
    public function setNotificationHttpMethod($method)
    {
        $this->notificationHttpMethod = $method;
        return $this;
    }

    /**
     * Set the back callback url
     *
     * @param string $url
     * @return self
     */
    public function setBackUrl($url)
    {
        $this->backCallback = $url;
        return $this;
    }

    /**
     * Set the success callback url
     *
     * @param string $url
     * @return self
     */
    public function setSuccessUrl($url)
    {
        $this->successCallback = $url;
        return $this;
    }

    /**
     * Set the failure callback url
     *
     * @param string $url
     * @return self
     */
    public function setFailureUrl($url)
    {
        $this->failureCallback = $url;
        return $this;
    }

    /**
     * Specify whether to return user billing information when the checkout has been completed
     *
     * @param boolean $bool
     * @return self
     */
    public function setSendReceiverAddress($bool)
    {

        $this->sendReceiverAddress = $bool;
        return $this;

    }

}