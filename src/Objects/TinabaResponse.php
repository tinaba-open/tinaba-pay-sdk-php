<?php
/**
 * Created by PhpStorm.
 * User: desh
 * Date: 15/03/18
 * Time: 12.51
 */

namespace Tinaba\Pay\Objects;


use Tinaba\Pay\Base\ApiObject;

/**
 * Class TinabaResponse
 * @package Tinaba\Pay\Objects
 *
 * @property string $status
 * @property string $errorCode
 */

class TinabaResponse extends ApiObject
{

    const STATUS_OK = '000';
    const STATUS_KO = '001';

    protected $attributes = [
        'status' => null,
        'errorCode' => null
    ];

}