<?php

namespace Tinaba\Pay;


use Tinaba\Pay\Base\Credentials;

class TinabaCredentials extends Credentials
{


    public function __construct($merchantId, $secret)
    {
        parent::__construct($merchantId, $secret);
    }

    public function getMerchantId()
    {
        return $this->getUsername();
    }

    public function getSecret()
    {
        return $this->getPassword();
    }


}