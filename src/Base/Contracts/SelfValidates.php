<?php

namespace Tinaba\Pay\Base\Contracts;


interface SelfValidates
{

    public static function getValidationRules();

}