<?php

namespace Tinaba\Pay\Base\Contracts;


interface Parsable
{

    public static function parse(array $attributes);

}