<?php

namespace Tinaba\Pay\Base\Contracts;


interface Relationable
{

    public static function getRelations();

}