<?php namespace Tinaba\Pay\Base;

use Illuminate\Support\Arr;
use Tinaba\Pay\Exceptions\BaseException;

class Factory
{
    /**
     * Mapping between action names and action classes
     * @var string
     */
    protected $actionsNamespace;

    /**
     * @var array
     */
    protected $actions = [];

    /**
     * @var ApiContext
     */
    protected $context;

    /**
     * itTaxiAPI constructor.
     *
     * @param ApiContext $context
     */
    public function __construct(ApiContext $context)
    {
        $this->context = $context;
    }


    /**
     * @param $entity
     *
     * @return \Tinaba\Pay\Base\Action
     * @throws
     */
    public function make($entity)
    {
        $restEntity = $this->getActionClass($entity);

        return new $restEntity($this->context);
    }

    /**
     * @param $entity
     *
     * @return mixed
     * @throws
     */
    protected function getActionClass($entity)
    {
        self::validateAction($entity);

        $actionClass = Arr::get($this->actions, $entity);
        return $this->actionsNamespace . '\\' . $actionClass;
    }

    /**
     * @param $entity
     *
     * @throws BaseException
     */
    protected function validateAction($entity)
    {
        if (!Arr::has($this->actions, $entity))
            throw new BaseException('Factory error, action not instantiable');
    }
}