<?php namespace Tinaba\Pay\Base;

use Illuminate\Translation\Translator;
use Illuminate\Validation\Factory as ValidationFactory;
use Illuminate\Contracts\Validation\Factory as ValidationFactoryInterface;

abstract class ApiContext
{
    /**
     * @var string
     */
    protected $sandbox;

    /**
     * @var int
     */
    protected $timeout;

    /**
     * @var string
     */
    protected $baseUrl;

    /**
     * @var array
     */
    protected $credentials;

    /**
     * @var ValidationFactoryInterface
     */
    protected $validation;

    /**
     * ApiContext constructor.
     * @param array $config
     */
    public function __construct(array $config)
    {
        $this->sandbox = $config['sandbox'];
        $this->timeout = $config['timeout'];
        $this->credentials = $this->buildCredentials($config);
        $this->makeValidationFactory();
    }

    /**
     * @param array $config Configuration file as an associative array
     * @return Credentials
     */
    protected abstract function buildCredentials(array $config);

    /**
     * @return void
     */
    protected function makeValidationFactory()
    {

        $loader = new ApiTranslationLoader();
        $translator = new Translator($loader, 'en');
        $this->validation = new ValidationFactory($translator);

    }

    /**
     * @param ValidationFactoryInterface $factory
     *
     */
    public function setValidationFactory(ValidationFactoryInterface $factory)
    {

        $this->validation = $factory;

    }

    /**
     * @return ValidationFactoryInterface
     */
    public function getValidationFactory()
    {
        return $this->validation;
    }

    /**
     * @return string
     */
    public function getBaseUrl()
    {
        return $this->baseUrl . '/';
    }

    /**
     * @return int
     */
    public function getTimeout()
    {
        return $this->timeout;
    }

    /**
     * @return string
     */
    public function isSandbox()
    {
        return $this->sandbox;
    }

    /**
     * @return Credentials
     */
    public function getCredentials()
    {
        return $this->credentials;
    }

}