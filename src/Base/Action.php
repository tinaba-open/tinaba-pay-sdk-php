<?php namespace Tinaba\Pay\Base;

use Carbon\Carbon;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use GuzzleHttp\Psr7\Request as Psr7Request;
use Tinaba\Pay\Exceptions\ArgumentsException;
use Tinaba\Pay\Exceptions\BaseException;
use Tinaba\Pay\Exceptions\ConnectionException;
use Tinaba\Pay\Exceptions\TinabaError;
use Tinaba\Pay\Exceptions\UrlParametersException;

abstract class Action
{

    /**
     * @var ApiContext
     */
    protected $context;

    /**
     * Array that holds all the parameters that will be inserted into the request body, according to the payload type.
     * @var array
     */
    protected $request_params = [];

    /**
     * Array that holds the url parameters that must be specified to get a valid endpoint
     */
    protected $url_params = [];

    /**
     * Original Request
     * @var Request
     */
    protected $request = null;

    /**
     * Original response
     * @var Response
     */
    protected $response = null;


    /**
     * Check whether the action has a response
     *
     * @return bool
     */
    public function hasResponse()
    {
        return !is_null($this->response);
    }

    /**
     * Returns the received response body
     *
     * @var bool $asArray
     *
     * @return array|string
     */
    public function getResponseOriginalBody($asArray = false)
    {

        $this->response->getRawResponse()->getBody()->rewind();
        $body = $this->response->getRawResponse()->getBody()->getContents();

        return $asArray ? json_decode($body, true) : $body;

    }

    /**
     * Returns the original response
     *
     * @return Response|null
     */
    public function getResponse()
    {
        return $this->response;
    }

    /**
     * Returns the sent request body
     *
     * @var bool $asArray
     *
     * @return array|string
     */
    public function getRequestOriginalBody($asArray = false)
    {

        $this->request->getRawRequest()->getBody()->rewind();
        $body = $this->request->getRawRequest()->getBody()->getContents();

        return $asArray ? json_decode($body, true) : $body;

    }

    /**
     * Returns the original request
     *
     * @return Request|null
     */
    public function getRequest()
    {
        return $this->request;
    }

    /**
     * Leaf classes must override that attribute with the relative endpoint url.
     * @return string
     */
    protected abstract function getEndpoint();

    /**
     * Leaf classes must override that attribute with the http method.
     * @return string
     */
    protected abstract function getMethod();

    /**
     * Leaf classes must override that attribute with the needed base headers.
     * @return array
     */
    protected abstract function getHeaders();

    /**
     * Leaf classes must override that attribute with the validation rules for the request parameters
     * @return array
     */
    protected abstract function getRequestParamsRules();

    /**
     * Leaf classes must override that attribute with the validation rules for the response parameters
     * @return array
     */
    protected abstract function getResponseParamsRules();

    /**
     * Leaf classes must override this function with the validation rules for the request endpoint
     */
    protected abstract function getUrlParametersRules();

    /**
     * Defines the signature clear text to hash
     *
     * @return string
     */
    protected abstract function getSignatureBody();

    /**
     * Defines the request name
     *
     * @return string
     */
    protected abstract function getRequestName();

    /**
     * Defines the response name
     *
     * @return string
     */
    protected abstract function getResponseName();

    /**
     * Supported payload types:
     * - raw (http://docs.guzzlephp.org/en/latest/request-options.html#body):
     * - form_params (http://docs.guzzlephp.org/en/latest/request-options.html#form_params):
     * - multipart (http://docs.guzzlephp.org/en/latest/request-options.html#multipart)
     * - json (http://docs.guzzlephp.org/en/latest/request-options.html#json)
     *
     * @return string
     */
    protected abstract function getPayloadType();

    /**
     * Parse the response, should be useful to instantiate an object.
     * @param $response
     * @return mixed
     */
    protected abstract function parseResponse($response);

    /**
     * BaseEndpoint constructor.
     * @param ApiContext $context
     */
    public function __construct(ApiContext $context)
    {
        $this->context = $context;

    }

    /**
     *
     * Base request parameter getter
     * @param string $attribute
     * @param mixed $default
     *
     * @return mixed
     */
    public function getRequestParameter($attribute, $default = null)
    {
        return Arr::get($this->request_params, $attribute, $default);
    }

    /**
     * Base url parameter getter
     * @param string $key
     * @param mixed $default
     *
     * @return mixed
     */
    public function getUrlParameter($key, $default = null)
    {
        return Arr::get($this->url_params, $key, $default);
    }

    /**
     * Checks if the action has the specified url parameter
     *
     * @param $key
     * @return bool
     */
    public function hasUrlParameter($key)
    {

        return in_array($key, $this->url_params, true);

    }

    /**
     * Checks if the action has the specified url parameter
     *
     * @param $key
     * @return bool
     */
    public function hasRequestParameter($key)
    {

        return Arr::has($this->request_params, $key) && $this->request_params[$key];

    }


    /**
     * Sets the specified url parameter to the provided value
     *
     * @param $key
     * @param $value
     */
    public function setUrlParameter($key, $value)
    {
        Arr::set($this->url_params, $key, $value);
    }

    /**
     * Sets the specified request parameter to the provided value
     *
     * @param $key
     * @param $value
     */
    public function setRequestParameter($key, $value)
    {
        Arr::set($this->request_params, $key, $value);
    }

    /**
     * @param int|Carbon $timestamp
     * @return int
     */
    public function parseTimestamp($timestamp)
    {
        if($timestamp instanceof Carbon)
        {
            return $timestamp->timestamp;
        }

        return $timestamp;
    }

    /**
     * Take a request and add authentication data
     * @param Psr7Request $request
     * @return Psr7Request $request
     */
    protected abstract function authenticate(Psr7Request $request);

    protected function getHttpAuthCredentials()
    {
        return [];
    }

    /**
     * @return callable
     */
    protected function getAuthenticateCallable()
    {
        return function(Psr7Request $request) { return $this->authenticate($request); };
    }

    /**
     * @param array $params
     * @param array $rules
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function makeValidator(array $params, array $rules)
    {

        return $this->context->getValidationFactory()->make($params, $rules);

    }

    /**
     * @param array $params
     * @param array $rules
     * @throws UrlParametersException
     */
    protected function validateUrlParameters(array $params, array $rules)
    {
        $validator = $this->makeValidator($params, $rules);
        if ($validator && $validator->fails())
            throw new UrlParametersException($validator);
    }

    /**
     * @param array $params
     * @param array $rules
     * @throws ArgumentsException
     */
    protected function validateBodyParameters(array $params, array $rules)
    {

        $validator = $this->makeValidator($params, $rules);
        if($validator && $validator->fails())
            throw new ArgumentsException($validator);

    }

    /**
     * That function must return an array, to be validated against response_params_rules.
     *
     * @param $body
     * @return array
     */
    protected function parseRawBody($body)
    {

        return json_decode($body, true);

    }

    /**
     * Builds the request payload
     *
     * @return array
     */
    protected function buildPayload()
    {
        return [
            'data' => [
                'request' => [
                    $this->getRequestName() => $this->request_params
                ]
            ]
        ];
    }

    protected function buildUrl()
    {

        $url = $this->getEndpoint();
        foreach($this->url_params as $urlParameter => $value)
        {
            $url = str_replace('{' . $urlParameter . '}', $value, $url);
        }

        return $url;

    }

    /**
     * @return mixed
     * @throws TinabaError|ArgumentsException|UrlParametersException|BaseException|ConnectionException|\GuzzleHttp\Exception\GuzzleException
     */
    public function send()
    {
        $this->validateUrlParameters($this->url_params, $this->getUrlParametersRules());
        $this->validateBodyParameters($this->request_params, $this->getRequestParamsRules());

        $request = $this->makeRequest();
        $this->response = $request->run();
        $this->request = $request;

        if(!$this->response->isEmpty()) {
            $parsed_response = $this->parseRawBody(
                $this->response->getRawBody()
            );
        }else{
            $parsed_response = [];
        }

        $this->checkForErrors($parsed_response['response'][$this->getResponseName()]);

        return $this->parseResponse($parsed_response['response'][$this->getResponseName()]);
    }

    /**
     *
     * @param array $payload
     * @throws
     */
    public function checkForErrors(array $payload)
    {

        if(isset($payload) && $payload['status'] !== '000')
        {

            throw TinabaError::parse($payload['status'], $payload['errorCode']);

        }

    }

    /**
     * Prepare the request
     * @return Request
     */
    protected function makeRequest()
    {
        return (new Request(
            $this->context->getBaseUrl(),
            $this->context->getTimeout(),
            $this->getPayloadType(),
            $this->getMethod(),
            $this->buildUrl(),
            $this->getHeaders(),
            $this->buildPayload(),
            $this->getAuthenticateCallable(),
            $this->getHttpAuthCredentials()
        ));
    }

    /**
     * Check if we are trying to get an url parameter
     *
     * @param $key
     * @return mixed
     */
    public function __get($key)
    {

        $method = 'get' . Str::studly($key);
        if(method_exists($this, $method)) {

            return $this->$method();

        }else if($this->hasUrlParameter($key)){
            return $this->getUrlParameter($key);
        }

        return null;
    }

    /**
     * Check if we are trying to set an url parameter
     *
     * @param $key
     * @param $value
     * @throws \Exception
     */
    public function __set($key, $value)
    {

        $method = 'set' . Str::studly($key);
        if(method_exists($this, $method)) {
            $this->setUrlParameter($key, $this->$method($value));
        }else if($this->hasUrlParameter($key)){
            $this->setUrlParameter($key, $value);
        }else {
            throw new \Exception("Undefined property '$key' in class " . get_class($this));
        }

        return;

    }
}