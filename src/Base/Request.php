<?php namespace Tinaba\Pay\Base;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Handler\CurlHandler;
use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\Middleware;
use Psr\Http\Message\RequestInterface;
use Tinaba\Pay\Exceptions\BaseException;
use Tinaba\Pay\Exceptions\ResponseException;
use Tinaba\Pay\Exceptions\ConnectionException;
use GuzzleHttp\Psr7\Response as GuzzleResponse;

class Request
{

    /**
     * @var GuzzleClient
     */
    protected $client;

    /**
     * @var RequestInterface
     */
    protected $rawRequest;

    /**
     * @var string
     */
    protected $base_url;

    /**
     * @var int
     */
    protected $timeout;

    /**
     * @var string
     */
    protected $payload_type;

    /**
     * @var callable
     */
    protected $authenticate;

    /**
     * @var string
     */
    protected $method;

    /**
     * @var string
     */
    protected $endpoint;

    /**
     * @var array
     */
    protected $headers = [];

    /**
     * @var array
     */
    protected $auth = [];

    /**
     * Request constructor.
     * @param $base_url
     * @param $timeout
     * @param $payload_type
     * @param $method
     * @param $endpoint
     * @param array $headers
     * @param $payload
     * @param callable $authenticate
     * @param array $auth
     */
    public function __construct($base_url, $timeout, $payload_type, $method, $endpoint, array $headers, $payload, callable $authenticate, $auth = [])
    {
        $this->base_url = $base_url;
        $this->timeout = $timeout;
        $this->payload_type = $payload_type;
        $this->method = $method;
        $this->endpoint = $endpoint;
        $this->headers = $headers;
        $this->payload = $payload;
        $this->authenticate = $authenticate;
        $this->auth = $auth;
    }

    /**
     * That method is the middleware used by guzzle to add authentication to the request.
     *
     * @param callable $authenticate
     * @return callable
     */
    protected function authenticate(callable $authenticate)
    {
        return Middleware::mapRequest(function(RequestInterface $request) use($authenticate){

            return $this->rawRequest = $authenticate($request);

        });

    }

    /**
     * @return array
     */
    protected function buildHeaders()
    {
        return $this->headers;
    }

    /**
     * @param array $options
     * @throws BaseException
     */
    protected function buildBody(array &$options)
    {
        if (empty($this->payload))
            return;

        switch ($this->payload_type){
            case 'json':
            case 'raw':
            case 'form_params':
            case 'multipart':
                $options[$this->payload_type] = $this->payload;
                break;

            default:
                throw new BaseException('Unknown payload type');
        }
    }

    /**
     * Builds the client needed to send the request.
     * @return GuzzleClient
     * @throws \Exception
     */
    protected function buildClient()
    {
        $stack = new HandlerStack();
        $stack->setHandler(new CurlHandler());
        $stack->push($this->authenticate($this->authenticate));

        $options = [
            'timeout'  => $this->timeout,
            'base_uri' => $this->base_url,
            'headers' => $this->buildHeaders(),
            'handler' => $stack,
            'http_errors' => false
        ];
        $this->buildBody($options);

        return new GuzzleClient($options);
    }

    /**
     * Builds a mocked Guzzle Client
     *
     * @param \GuzzleHttp\Psr7\Response $response
     * @param array $args
     *
     * @return \Mockery\Mock
     * @throws \Exception
     */
    public static function mock(\GuzzleHttp\Psr7\Response $response, ...$args)
    {

        $mockedRequest = \Mockery::mock(self::class, $args)->makePartial();

        $mockedRequest->shouldAllowMockingProtectedMethods()
            ->shouldReceive('buildClient')
            ->andReturnUsing(function() use($response, $mockedRequest) {

                $mockedHandler = new MockHandler([
                    $response
                ]);

                $stack = HandlerStack::create($mockedHandler);
                $stack->push($mockedRequest->authenticate($mockedRequest->authenticate));

                $options = [
                    'timeout' => $mockedRequest->timeout,
                    'base_uri' => $mockedRequest->base_url,
                    'headers' => $mockedRequest->buildHeaders(),
                    'handler' => $stack,
                    'http_errors' => false
                ];

                $mockedRequest->buildBody($options);

                return new GuzzleClient($options);
            });

        return $mockedRequest;
    }

    /**
     * @return Response
     * @throws \Exception|BaseException|ConnectionException|\GuzzleHttp\Exception\GuzzleException
     */
    public function run()
    {
        $this->client = $this->buildClient();
        $start_time = round(microtime(true) * 1000);

        try {
            $response = $this->client->request($this->method, $this->endpoint);
            $this->checkStatus($response);

        } catch (\Exception $e) {
            if(!$e instanceof BaseException) {
                throw new BaseException("Unknown exception", BaseException::UNKNOWN_ERROR, $e);
            }

            throw $e;
        }

        $end_time = round(microtime(true) * 1000);

        return new Response($this, $response, $end_time - $start_time);
    }

    /**
     * Returns the raw request sent to the server
     *
     * @return RequestInterface
     */
    public function getRawRequest()
    {
        return $this->rawRequest;
    }

    /**
     *
     * Check if the response is successful
     *
     * @param GuzzleResponse $response
     * @throws ConnectionException
     * @throws ResponseException
     */
    protected function checkStatus(GuzzleResponse $response)
    {
        $status_code = $response->getStatusCode();
        $body = $response->getBody()->getContents();
        $response->getBody()->rewind();
        if($status_code >= 400 && $status_code < 500){
            throw new ResponseException($status_code, $body);
        }else if($status_code >= 500){
            throw new ConnectionException($status_code, $body);
        }
    }
}