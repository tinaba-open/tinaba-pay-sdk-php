<p align="center"><img src ="https://www.tinaba.com/wp-content/themes/tinaba/img/logos/logo-horizontal.svg" width=300 height=200 /></p>

# Tinaba Pay SDK (PHP)

This package provides the integration with the Tinaba Pay API for PHP.
Moreover this package can also be included as a **Laravel** package.

## Requirements

- PHP >= 5.6.4
- [GuzzleHttp 6](http://docs.guzzlephp.org/en/stable/)
- [Carbon](https://carbon.nesbot.com/docs/)
- [illuminate/validation >= 5.4.0](https://github.com/illuminate/validation)

If you are using composer this requirements will be automatically installed

## API reference

<a href="https://www.tinaba.com/it/developers/">Here</a> you can find the official API documentation.

## Quick Start

In order to successfully communicate with Tinaba Pay APIs you need to
request your **Merchant ID** and **Secret** to Tinaba.

### Installing using Composer

`composer require tinaba/pay-sdk`

#### Configuring the client

To setup the API Client you have to set the Client configuration:

```php
<?php

require 'vendor/autoload.php';

use \Tinaba\Pay\ApiContext;
use \Tinaba\Pay\Client;

$config = [
    'merchantId' => "TINABA_MERCHANT_ID",
    'secret' => 'TINABA_SECRET',
    'timeout' => 20 // request timeout in seconds
];

$context = new ApiContext($config);

$client = new Client($context);
```

#### Sandbox Mode

To operate in sandbox mode set the field `sandbox` to `true` when
instantiating the `ApiContext` object.

```php
<?php

$config = [
    'sandbox' => true,
    'merchantId' => "TINABA_MERCHANT_ID",
    'secret' => 'TINABA_SECRET',
    'timeout' => 20 // request timeout in seconds
];

$context = new ApiContext($config);

$client = new Client($context);
```

## Examples

Once you have successfully managed to setup the SDK you can start making
API calls to the Tinaba Pay platform.

### Initialize a Checkout

```php
<?php

$initCheckoutRequest = new InitCheckoutRequest();

$initCheckoutRequest->setExternalId('TR_01')
    ->setAmount('100') // Expressed in cents
    ->setCurrency('EUR')
    ->setDescription('Customized Mug purchase')
    ->setValidTo('10') // Expressed in minutes
    ->setCreationDateTime(Carbon::now())
    ->setPaymentMode(InitCheckoutRequest::MODE_ECOMMERCE)
    ->setNotificationUrl('https://example.com/TR_01/status')
    ->setNotificationHttpMethod('POST')
    ->setBackUrl('https://example.com/paymentcanceled')
    ->setSuccessUrl('https://example.com/paymentsuccess')
    ->setFailureUrl('https://example.com/paymentfailed');

// The API Client instantiated before
$response = $client->initCheckout($initCheckoutRequest);

echo "Payment code: " . $response->code;
```

Available payment modes are

```php
<?php

InitCheckoutRequest::MODE_PREAUTH;
InitCheckoutRequest::MODE_ECOMMERCE;
InitCheckoutRequest::MODE_MEDIA;
```

#### Enable One-Click mode

To enable *one-click* mode you have to set the `sendReceiverAddress` to `true` when initializing the checkout.

```php
<?php

$initCheckoutRequest = new InitCheckoutRequest();

$initCheckoutRequest->setExternalId('TR_01')
    ->setAmount('100') // Expressed in cents
    ->setCurrency('EUR')
    ->setDescription('Customized Mug purchase')
    ->setValidTo('10') // Expressed in minutes
    ->setCreationDateTime(Carbon::now())
    ->setPaymentMode(InitCheckoutRequest::MODE_ECOMMERCE)
    ->setNotificationUrl('https://example.com/TR_01/status')
    ->setNotificationHttpMethod('POST')
    ->setBackUrl('https://example.com/paymentcanceled')
    ->setSuccessUrl('https://example.com/paymentsuccess')
    ->setFailureUrl('https://example.com/paymentfailed')
    ->setSendReceiverAddress(true);

// The API Client instantiated before
$response = $client->initCheckout($initCheckoutRequest);

echo "Payment code: " . $response->code;
```

If `sendReceiverAddress` is enabled you will receive also the customer
shipping and billing details when performing a `VerifyCheckoutRequest`
or when you receive a `CheckoutStateCallback`.

### Confirm a Preauthorized checkout

```php
<?php

$request = new ConfirmPreauthorizedCheckoutRequest();

$request->setExternalId('TR_01')
    ->setAmount('100'); // Amount expressed in cents

$response = $client->confirmPreauthorizedCheckout($request);

```

### Verify the checkout state


```php
<?php

$request = new VerifyCheckoutRequest();

$request->setExternalId('TR_01');

$response = $client->verifyCheckout($request);

echo "The checkout status is " . $response->checkoutStatus;
```

### Perform a full/partial refund of a checkout

```php
<?php

$request = new RefundCheckoutRequest();

$request->setExternalId('TR_01');
$request->setAmount('100'); // Amount expressed in cents

$response = $client->refundCheckout($request);
```

### Get the list of generated checkouts

```php
<?php

$request = new GetCheckoutListRequest();

$request->setDateFrom('2018', '01', '01');
$request->setDateTo('2018', '02', '01');

$response = $client->getCheckoutList($request);

$checkoutList = $request->checkoutList;

foreach($checkoutList as $checkout) {

    echo "Found checkout with ID " . $checkout->externalId;

}

```

### Verify a callback

```php
<?php

// The request body as an associative array
$requestBody = json_decode($request->body, true);

$callbackRequest = CheckoutStateCallback::parse($request->body)

$valid = $client->verifyCallback($callbackRequest);

if($valid) {
    $successResponse = new CallbackSuccessResponse();

    // Return the response as json
    $responseBody = json_encode($successResponse->toArray());

    /*
     * Send the response as JSON with $responseBody as payload.
     * The following line is for example purpose only.
     */
    return response()->json($responseBody, 200);
}else {
    // Handle error
}

```

### Check if a call was successful

```php
<?php

if($response->status === TinabaResponse::STATUS_OK) {
    echo "Confirmation successful";
}else {
    echo "Confirmation failed with error code " . $response->errorCode;
}
```

## Usage in Laravel

This package supports Laravel 5.4+ .

### Setup

Include the `TinabaPayServiceProvider` in your `config/app.php` file
under the `providers` key.

```php
<?php

// in config/app.php

'providers' => [

    // ... other providers
    \Tinaba\Pay\Laravel\TinabaPayServiceProvider::class,
],

```

publish the configuration file in your `config` directory

```

$ php artisan vendor:publish --provider=\Tinaba\Pay\Laravel\TinabaServiceProvider

```

**OPTIONAL** add the alias in your `config/app.php` under the `aliases` key

```
<?php

// in config/app.php

'aliases' => [
    // ... other aliases
    'TinabaPay' => \Tinaba\Pay\Laravel\Facades\TinabaPay::class,
],

```

### Usage

```php
<?php

// Using the container
$response = app('tinaba.pay')->initCheckout($request);

// Using the Facade

$response = TinabaPay::initCheckout($request);

```
