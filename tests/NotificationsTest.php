<?php

namespace Tinaba\Pay\Test;


use Tinaba\Pay\Objects\BillingAddress;
use Tinaba\Pay\Objects\CheckoutStateCallback;
use Tinaba\Pay\Objects\ShippingAddress;
use Tinaba\Pay\Objects\UserAddress;

class NotificationsTest extends TestCase
{

    public function testCallbackSuccess()
    {

        $externalId = 'EXT_123';
        $checkoutState = '000';
        $secret = $this->apiContext->getCredentials()->getSecret();

        $signature = $externalId.$checkoutState.$secret;
        $signature = base64_encode(hash('sha256', $signature, true));

        $request = [
            'externalId' => $externalId,
            'checkoutState' => $checkoutState,
            'signature' => $signature
        ];

        $callback = CheckoutStateCallback::parse($request);
        
        $this->assertInstanceOf(CheckoutStateCallback::class, $callback);
        $this->assertEquals($externalId, $callback->externalId);
        $this->assertEquals($checkoutState, $callback->checkoutState);
        
        $this->assertTrue($this->client->verifyCallback($callback));


    }
    
    public function testCallbackSuccessWithReceiverInfo()
    {

        $externalId = 'EXT_123';
        $checkoutState = '000';
        $secret = $this->apiContext->getCredentials()->getSecret();

        $signature = $externalId.$checkoutState.$secret;
        $signature = base64_encode(hash('sha256', $signature, true));

        $request = [
            'externalId' => $externalId,
            'checkoutState' => $checkoutState,
            'signature' => $signature,
            'userAddress' => [
                'name' => 'John',
                'surname' => 'Doe',
                'email' => 'john.doe@example.com',
                'shippingAddress' => [
                    'receiverName' => 'John Doe',
                    'address' => 'Via del Corso',
                    'streetNumber' => '11',
                    'city' => 'Roma',
                    'cap' => '00100',
                    'district' => 'RM',
                    'country' => 'Italia',
                    'sendAt' => 'Dummy Business Center',
                    'phoneNumber' => '+393281234567'
                ],
                'billingAddress' => [
                    'receiverName' => 'John Doe',
                    'address' => 'Via Veneto',
                    'streetNumber' => '15',
                    'city' => 'Roma',
                    'cap' => '00100',
                    'district' => 'RM',
                    'country' => 'Italia',
                    'fiscalCode' => 'JHNDOE90R24H501K'
                ]
            ]
        ];

        $callback = CheckoutStateCallback::parse($request);

        $this->assertInstanceOf(CheckoutStateCallback::class, $callback);
        $this->assertEquals($externalId, $callback->externalId);
        $this->assertEquals($checkoutState, $callback->checkoutState);

        $this->assertTrue($this->client->verifyCallback($callback));

        $this->assertInstanceOf(UserAddress::class, $callback->userAddress);
        $this->assertEquals('John', $callback->userAddress->name);
        $this->assertEquals('Doe', $callback->userAddress->surname);

        $this->assertInstanceOf(ShippingAddress::class, $callback->userAddress->shippingAddress);
        $this->assertEquals('John Doe', $callback->userAddress->shippingAddress->receiverName);
        $this->assertEquals('Via del Corso', $callback->userAddress->shippingAddress->address);
        $this->assertEquals('11', $callback->userAddress->shippingAddress->streetNumber);
        $this->assertEquals('Roma', $callback->userAddress->shippingAddress->city);
        $this->assertEquals('00100', $callback->userAddress->shippingAddress->cap);
        $this->assertEquals('RM', $callback->userAddress->shippingAddress->district);
        $this->assertEquals('Italia', $callback->userAddress->shippingAddress->country);
        $this->assertEquals('Dummy Business Center', $callback->userAddress->shippingAddress->sendAt);
        $this->assertEquals('+393281234567', $callback->userAddress->shippingAddress->phoneNumber);

        $this->assertInstanceOf(BillingAddress::class, $callback->userAddress->billingAddress);
        $this->assertEquals('John Doe', $callback->userAddress->billingAddress->receiverName);
        $this->assertEquals('Via Veneto', $callback->userAddress->billingAddress->address);
        $this->assertEquals('15', $callback->userAddress->billingAddress->streetNumber);
        $this->assertEquals('Roma', $callback->userAddress->billingAddress->city);
        $this->assertEquals('00100', $callback->userAddress->billingAddress->cap);
        $this->assertEquals('RM', $callback->userAddress->billingAddress->district);
        $this->assertEquals('Italia', $callback->userAddress->billingAddress->country);
        $this->assertEquals('JHNDOE90R24H501K', $callback->userAddress->billingAddress->fiscalCode);
        
    }

    public function testCallbackVerifyFailure()
    {

        $externalId = 'EXT_123';
        $checkoutState = '000';
        $secret = str_random(32);

        $signature = $externalId.$checkoutState.$secret;
        $signature = base64_encode(hash('sha256', $signature, true));

        $request = [
            'externalId' => $externalId,
            'checkoutState' => $checkoutState,
            'signature' => $signature
        ];

        $callback = CheckoutStateCallback::parse($request);

        $this->assertInstanceOf(CheckoutStateCallback::class, $callback);
        $this->assertEquals($externalId, $callback->externalId);
        $this->assertEquals($checkoutState, $callback->checkoutState);

        $this->assertFalse($this->client->verifyCallback($callback));

    }

}