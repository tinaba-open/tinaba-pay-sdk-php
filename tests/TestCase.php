<?php

namespace Tinaba\Pay\Test;


use Tinaba\Pay\ApiContext;
use Tinaba\Pay\MockedClient;
use PHPUnit\Framework\TestCase as BaseTestCase;

class TestCase extends BaseTestCase
{

    /**
     * @var MockedClient
     */
    protected $client;

    /**
     * @var ApiContext
     *
     */
    protected $apiContext;

    public function setUp()
    {

        $this->bootstrapClient();

    }

    protected function bootstrapClient()
    {

        $config = [
            'sandbox' => true,
            'secret' => 'secret',
            'merchantId' => 'merchantId'
        ];

        $this->apiContext = new ApiContext($config);
        $this->client = new MockedClient($this->apiContext);

    }

    protected function refreshClient()
    {

        $this->bootstrapClient();

    }

}