<?php

namespace Tinaba\Pay\Test;


use Carbon\Carbon;
use Tinaba\Pay\Objects\InitCheckoutRequest;
use Tinaba\Pay\Objects\InitCheckoutResponse;
use Tinaba\Pay\Objects\TinabaResponse;

class InitCheckoutTest extends TestCase
{


    protected function makeDummyRequest()
    {

        return (new InitCheckoutRequest())
            ->setExternalId('EXT_123')
            ->setAmount('12345')
            ->setCurrency('EUR')
            ->setDescription('This is a test description')
            ->setValidTo('10')
            ->setCreationDateTime(Carbon::now())
            ->setPaymentMode('ECOMMERCE')
            ->setMetadata(json_encode([
                'hello' => 'world'
            ]))
            ->setNotificationUrl('http://example.com')
            ->setNotificationHttpMethod('GET')
            ->setBackUrl('http://example.com/canceled')
            ->setSuccessUrl('http://example.com/success')
            ->setFailureUrl('http://example.com/failed')
            ->setSendReceiverAddress(true);

    }

    public function testInitSuccess()
    {

        $request = $this->makeDummyRequest();

        $dummyPaymentCode = 'ABCDEFGHI';

        $this->client->setMockedStatusCode(200)
            ->setMockedBody([
                'response' => [
                    'initCheckoutResponse' => [
                        'status' => '000',
                        'paymentCode' => $dummyPaymentCode
                    ]
                ]
            ]);

        $response = $this->client->initCheckout($request);

        $this->assertInstanceOf(InitCheckoutResponse::class, $response);
        $this->assertEquals($response->status, TinabaResponse::STATUS_OK);
        $this->assertEquals($response->paymentCode, $dummyPaymentCode);

    }


}