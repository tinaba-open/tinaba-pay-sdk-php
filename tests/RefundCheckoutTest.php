<?php

namespace Tinaba\Pay\Test;


use Tinaba\Pay\Objects\RefundCheckoutRequest;
use Tinaba\Pay\Objects\RefundCheckoutResponse;

class RefundCheckoutTest extends TestCase
{

    public function testRefundCheckoutSuccess()
    {

        $request = (new RefundCheckoutRequest())
            ->setExternalId('EXT_123')
            ->setAmount('12345');

        $this->client->setMockedStatusCode(200)
            ->setMockedBody([
                'response' => [
                    'refundCheckoutResponse' => [
                        'status' => '000'
                    ]
                ]
            ]);

        $response = $this->client->refundCheckout($request);

        $this->assertInstanceOf(RefundCheckoutResponse::class, $response);
        $this->assertEquals('000', $response->status);

    }

}