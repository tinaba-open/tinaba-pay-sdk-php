<?php

namespace Tinaba\Pay\Test;


use Tinaba\Pay\Objects\ConfirmPreauthorizedCheckoutRequest;
use Tinaba\Pay\Objects\ConfirmPreauthorizedCheckoutResponse;

class ConfirmPreauthorizedCheckoutTest extends TestCase
{


    public function testConfirmPreauthorizedCheckoutSuccess()
    {

        $request = (new ConfirmPreauthorizedCheckoutRequest())
            ->setExternalId('EXT_123')
            ->setAmount('23456');

        $this->client->setMockedStatusCode(200)
            ->setMockedBody([
                'response' => [
                    'confirmP2PtransferByCaptureResponse' => [
                        'confirmPreauthorizedCheckoutResponse' => [
                            'status' => '000'
                        ]
                    ]
                ]
            ]);

        $response = $this->client->confirmPreauthorizedCheckout($request);

        $this->assertInstanceOf(ConfirmPreauthorizedCheckoutResponse::class, $response);
        $this->assertEquals('000', $response->status);
    }


}