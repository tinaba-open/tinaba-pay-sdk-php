<?php

namespace Tinaba\Pay\Test;


use Carbon\Carbon;
use Tinaba\Pay\Exceptions\CheckoutCreationError;
use Tinaba\Pay\Exceptions\FormatError;
use Tinaba\Pay\Exceptions\MerchantConfigError;
use Tinaba\Pay\Exceptions\PaymentValidationError;
use Tinaba\Pay\Exceptions\RefundError;
use Tinaba\Pay\Exceptions\SignatureValidationError;
use Tinaba\Pay\Exceptions\TinabaError;
use Tinaba\Pay\Exceptions\UnknownError;
use Tinaba\Pay\Objects\ConfirmPreauthorizedCheckoutRequest;
use Tinaba\Pay\Objects\GetCheckoutListRequest;
use Tinaba\Pay\Objects\InitCheckoutRequest;
use Tinaba\Pay\Objects\RefundCheckoutRequest;
use Tinaba\Pay\Objects\VerifyCheckoutRequest;

class ErrorsTest extends TestCase
{



    /**
     * @dataProvider errorProvider
     */
    public function testInitCheckoutErrors($exceptionClass, $exceptionCode, $errorCode)
    {

        $request = (new InitCheckoutRequest())
            ->setExternalId('EXT_123')
            ->setAmount('12345')
            ->setCurrency('EUR')
            ->setDescription('This is a test description')
            ->setValidTo('10')
            ->setCreationDateTime(Carbon::now())
            ->setPaymentMode('ECOMMERCE')
            ->setMetadata(json_encode([
                'hello' => 'world'
            ]))
            ->setNotificationUrl('http://example.com')
            ->setNotificationHttpMethod('GET')
            ->setBackUrl('http://example.com/canceled')
            ->setSuccessUrl('http://example.com/success')
            ->setFailureUrl('http://example.com/failed')
            ->setSendReceiverAddress(true);

        $this->client->setMockedStatusCode(200)
            ->setMockedBody([
                'response' => [
                    'initCheckoutResponse' => [
                        'status' => '001',
                        'errorCode' => $errorCode
                    ]
                ]
            ]);

        $this->assertTinabaError($exceptionClass, $exceptionCode, function() use($request){
           $this->client->initCheckout($request);
        });

    }

    /**
     * @dataProvider errorProvider
     */
    public function testConfirmPreauthorizedCheckoutErrors($exceptionClass, $exceptionCode, $errorCode)
    {

        $request = (new ConfirmPreauthorizedCheckoutRequest())
            ->setExternalId('EXT_123')
            ->setAmount('12345');

        $this->client->setMockedStatusCode(200)
            ->setMockedBody([
                'response' => [
                    'confirmP2PtransferByCaptureResponse' => [
                        'confirmPreauthorizedCheckoutResponse' => [
                            'status' => '001',
                            'errorCode' => $errorCode
                        ]
                    ]
                ]
            ]);

        $this->assertTinabaError($exceptionClass, $exceptionCode, function() use($request){
            $this->client->confirmPreauthorizedCheckout($request);
        });

    }

    /**
     * @dataProvider errorProvider
     */
    public function testGetCheckoutListErrors($exceptionClass, $exceptionCode, $errorCode)
    {

        $request = (new GetCheckoutListRequest())
            ->setDateFrom('2017', '01', '01')
            ->setDateTo('2017', '12', '31');

        $this->client->setMockedStatusCode(200)
            ->setMockedBody([
                'response' => [
                    'getCheckoutListResponse' => [
                        'status' => '001',
                        'errorCode' => $errorCode
                    ]
                ]
            ]);

        $this->assertTinabaError($exceptionClass, $exceptionCode, function() use($request){
            $this->client->getCheckoutList($request);
        });

    }

    /**
     * @dataProvider errorProvider
     */
    public function testVerifyCheckoutErrors($exceptionClass, $exceptionCode, $errorCode)
    {

        $request = (new VerifyCheckoutRequest())
            ->setExternalId('EXT_123');

        $this->client->setMockedStatusCode(200)
            ->setMockedBody([
                'response' => [
                    'verifyCheckoutResponse' => [
                        'status' => '001',
                        'errorCode' => $errorCode
                    ]
                ]
            ]);

        $this->assertTinabaError($exceptionClass, $exceptionCode, function() use($request){
            $this->client->verifyCheckout($request);
        });

    }

    /**
     * @dataProvider errorProvider
     */
    public function testRefundCheckoutErrors($exceptionClass, $exceptionCode, $errorCode)
    {

        $request = (new RefundCheckoutRequest())
            ->setExternalId('EXT_123')
            ->setAmount('12345');

        $this->client->setMockedStatusCode(200)
            ->setMockedBody([
                'response' => [
                    'refundCheckoutResponse' => [
                        'status' => '001',
                        'errorCode' => $errorCode
                    ]
                ]
            ]);

        $this->assertTinabaError($exceptionClass, $exceptionCode, function() use($request){
            $this->client->refundCheckout($request);
        });

    }

    protected function assertTinabaError($exceptionClass, $exceptionCode, callable $callable)
    {
        $this->expectException($exceptionClass);
        $this->expectExceptionCode($exceptionCode);

        try {
            $callable();
        } catch (TinabaError $exception) {

            $this->assertEquals($exception->getStatus(), '001');
            throw $exception;

        }
    }

    public function errorProvider()
    {

        return [
            [
                UnknownError::class,
                TinabaError::UNKNOWN_ERROR,
                'IGE_0001'
            ],
            [
                FormatError::class,
                TinabaError::FORMAT_ERROR,
                'EPE_0001'
            ],
            [
                MerchantConfigError::class,
                TinabaError::MERCHANT_CONFIG_ERROR,
                'MCE_0001'
            ],
            [
                CheckoutCreationError::class,
                TinabaError::CHECKOUT_CREATION_ERROR,
                'CKE_0001'
            ],
            [
                PaymentValidationError::class,
                TinabaError::PAYMENT_VALIDATION_ERROR,
                'PYE_0001'
            ],
            [
                SignatureValidationError::class,
                TinabaError::SIGNATURE_VALIDATION_ERROR,
                'SGE_0001'
            ],
            [
                RefundError::class,
                TinabaError::REFUND_ERROR,
                'RFE_0001'
            ]
        ];

    }

}