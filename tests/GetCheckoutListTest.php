<?php

namespace Tinaba\Pay\Test;


use Carbon\Carbon;
use Tinaba\Pay\Objects\Checkout;
use Tinaba\Pay\Objects\GetCheckoutListRequest;
use Tinaba\Pay\Objects\GetCheckoutListResponse;

class GetCheckoutListTest extends TestCase
{

    public function testGetCheckoutListSuccess()
    {

        $request = (new GetCheckoutListRequest())
            ->setDateFrom(2018, 1, 1)
            ->setDateTo(2018, 12, 31);

        $dummyCheckouts = $this->generateDummyCheckouts();

        $this->client->setMockedStatusCode(200)
            ->setMockedBody([
                'response' => [
                    'getCheckoutListResponse' => [
                        'status' => '000',
                        'checkoutList' => array_values($dummyCheckouts)
                    ]
                ]
            ]);

        $response = $this->client->getCheckoutList($request);

        $this->assertInstanceOf(GetCheckoutListResponse::class, $response);
        $this->assertEquals('000', $response->status);

        $this->assertTrue(is_array($response->checkoutList));

        foreach($response->checkoutList as $checkout) {

            $this->assertInstanceOf(Checkout::class, $checkout);

            /**
             * @var Checkout $checkout
             */
            $this->assertArrayHasKey($checkout->externalId, $dummyCheckouts);
            $dummyCheckout = $dummyCheckouts[$checkout->externalId];

            $this->assertEquals($dummyCheckout['externalId'], $checkout->externalId);
            $this->assertEquals($dummyCheckout['merchantId'], $checkout->merchantId);
            $this->assertEquals($dummyCheckout['amount'], $checkout->amount);
            $this->assertEquals($dummyCheckout['currency'], $checkout->currency);
            $this->assertEquals($dummyCheckout['authTime'], $checkout->authTime->format('Y-m-d-H.i.s'));
            $this->assertEquals($dummyCheckout['transactionType'], $checkout->transactionType);
            $this->assertEquals($dummyCheckout['state'], $checkout->state);

        }

    }

    protected function generateDummyCheckouts()
    {

        $transactionTypes = ['PAYMENT', 'REFUND'];
        $states = ['EXECUTED', 'FAILED', 'PENDING', 'PREAUTH'];

        $rand = rand(10, 100);
        $checkouts = [];
        foreach (range(0, $rand) as $id) {

            $externalId = 'EXT_' . str_pad($id, 3, '0', STR_PAD_LEFT);

            $checkouts[$externalId] = [
                'externalId' => $externalId,
                'merchantId' => $this->apiContext->getCredentials()->getMerchantId(),
                'amount' => rand(100, 1000000),
                'currency' => 'EUR',
                'authTime' => Carbon::now()->format('Y-m-d-H.i.s'),
                'transactionType' => $transactionTypes[array_rand($transactionTypes)],
                'state' => $states[array_rand($states)],
                'internalTransactionId' => rand()
            ];

        }

        return $checkouts;
    }

}