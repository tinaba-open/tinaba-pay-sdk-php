<?php

namespace Tinaba\Pay\Test;

use Illuminate\Support\Arr;
use Tinaba\Pay\Exceptions\ArgumentsException;
use Tinaba\Pay\Objects\ConfirmPreauthorizedCheckoutRequest;
use Tinaba\Pay\Objects\GetCheckoutListRequest;
use Tinaba\Pay\Objects\InitCheckoutRequest;
use Tinaba\Pay\Objects\RefundCheckoutRequest;
use Tinaba\Pay\Objects\VerifyCheckoutRequest;

/**
 * Class ValidationTestCase
 * @package Tinaba\Pay\Test
 *
 */
class RequestValidationTest extends TestCase
{

    protected $standardPayloads = [
        InitCheckoutRequest::class => [
            'externalId' => 'EI_123',
            'amount' => '12345',
            'currency' => 'EUR',
            'creationDate' => '20180815',
            'creationTime' => '150200',
            'paymentMode' => 'ECOMMERCE',
            'notificationCallback' => 'http://test.url',
            'notificationHttpMethod' => 'GET'
        ],
        ConfirmPreauthorizedCheckoutRequest::class => [
            'externalId' => 'EI_123',
            'amount' => '123456',
        ],
        GetCheckoutListRequest::class => [
            'dateFrom' => '2018-02-01',
            'dateTo' => '2018-02-28'
        ],
        RefundCheckoutRequest::class => [
            'externalId' => 'EI_123',
            'amount' => '2345'
        ],
        VerifyCheckoutRequest::class => [
            'externalId' => 'EI_123'
        ]
    ];

    protected $clientActions = [
        InitCheckoutRequest::class => 'initCheckout',
        ConfirmPreauthorizedCheckoutRequest::class => 'confirmPreauthorizedCheckout',
        GetCheckoutListRequest::class => 'getCheckoutList',
        RefundCheckoutRequest::class => 'refundCheckout',
        VerifyCheckoutRequest::class => 'verifyCheckout'
    ];

    /**
     * @dataProvider initCheckoutRequestDataProvider
     */
    public function testInitCheckoutValidationRules($requestClass, $merge, $except)
    {

        $correctPayload = $this->standardPayloads[$requestClass];

        $body = Arr::except($merge + $correctPayload, $except);

        $request = $requestClass::parse($body);

        $this->expectException(ArgumentsException::class);

        $clientAction = $this->clientActions[$requestClass];

        $this->client->$clientAction($request);

    }

    public function initCheckoutRequestDataProvider()
    {

        return [

            // Missing externalId
            [
                InitCheckoutRequest::class,
                [],
                ['externalId']
            ],
            // Missing amount
            [
                InitCheckoutRequest::class,
                [],
                ['amount']
            ],
            // Missing currency
            [
                InitCheckoutRequest::class,
                [],
                ['currency']
            ],
            // Missing creationDate
            [
                InitCheckoutRequest::class,
                [],
                ['creationDate']
            ],
            // Wrong creationDate
            [
                InitCheckoutRequest::class,
                ['creationDate' => 'WRONG'],
                []
            ],
            // Wrong creationTime
            [
              InitCheckoutRequest::class,
              ['creationTime' => "WRONG"],
              []
            ],
            // Missing creationTime
            [
                InitCheckoutRequest::class,
                [],
                ['creationTime']
            ],
            // Missing paymentMode
            [
                InitCheckoutRequest::class,
                [],
                ['paymentMode']
            ],
            // Wrong paymentMode
            [
                InitCheckoutRequest::class,
                [ 'paymentMode' => 'WRONG' ],
                []
            ],
            // Missing notification callback
            [
                InitCheckoutRequest::class,
                [],
                ['notificationCallback']
            ],
            // Missing notification http method
            [
                InitCheckoutRequest::class,
                [],
                ['notificationHttpMethod']
            ],
            // Wrong notificationHttpMethod
            [
                InitCheckoutRequest::class,
                ['notificationHttpMethod' => 'WRONG'],
                []
            ],
            // Missing productCodeId
            [
                InitCheckoutRequest::class,
                [
                    'paymentMode' => 'MEDIA'
                ],
                []
            ],

            /* Confirm Checkout request */

            // Missing externalId
            [
                ConfirmPreauthorizedCheckoutRequest::class,
                [],
                ['externalId']
            ],
            // Missing amount
            [
                ConfirmPreauthorizedCheckoutRequest::class,
                [],
                ['amount']
            ],

            /* GetCheckoutListRequest */

            // Missing dateFrom
            [
                GetCheckoutListRequest::class,
                [],
                ['dateFrom']
            ],
            // Wrong dateFrom
            [
                GetCheckoutListRequest::class,
                ['dateFrom' => 'WRONG'],
                []
            ],
            // Missing dateTo
            [
                GetCheckoutListRequest::class,
                [],
                ['dateTo']
            ],
            // Wrong dateTo
            [
                GetCheckoutListRequest::class,
                ['dateTo' => 'WRONG'],
                []
            ],

            /* RefundCheckoutRequest */

            // Missing externalId
            [
                RefundCheckoutRequest::class,
                [],
                ['externalId']
            ],
            // Missing Amount
            [
                RefundCheckoutRequest::class,
                [],
                ['amount']
            ],

            /* VerifyCheckoutRequest*/
            // Missing externalId
            [
                VerifyCheckoutRequest::class,
                [],
                ['externalId']
            ]

        ];

    }


}