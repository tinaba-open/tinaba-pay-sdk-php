<?php

namespace Tinaba\Pay\Test;


use Tinaba\Pay\Objects\BillingAddress;
use Tinaba\Pay\Objects\ShippingAddress;
use Tinaba\Pay\Objects\UserAddress;
use Tinaba\Pay\Objects\VerifyCheckoutRequest;
use Tinaba\Pay\Objects\VerifyCheckoutResponse;

class VerifyCheckoutTest extends TestCase
{

    /**
     * @dataProvider checkoutStatusDataProvider
     */
    public function testVerifyCheckoutSuccess($checkoutStatus)
    {

        $request = (new VerifyCheckoutRequest())
            ->setExternalId('EXT_123');

        $response = [
            'response' => [
                'verifyCheckoutResponse' => [
                    'status' => '000',
                    'checkoutState' => $checkoutStatus,
                    'merchantId' => $this->apiContext->getCredentials()->getMerchantId(),
                    'externalId' => 'EXT_123',
                    'amount' => '100',
                    'currency' => 'EUR'
                ]
            ]
        ];

        $this->client->setMockedStatusCode(200)
            ->setMockedBody($response);

        $response = $this->client->verifyCheckout($request);

        $this->assertInstanceOf(VerifyCheckoutResponse::class, $response);
        $this->assertEquals('000', $response->status);
        $this->assertEquals($checkoutStatus, $response->checkoutState);
        $this->assertEquals('EXT_123', $response->externalId);
        $this->assertEquals('100', $response->amount);
        $this->assertEquals('EUR', $response->currency);

    }

    public function testVerifyCheckoutSuccessWithReceiverInfo()
    {

        $request = (new VerifyCheckoutRequest())
            ->setExternalId('EXT_123');

        $response = [
            'response' => [
                'verifyCheckoutResponse' => [
                    'status' => '000',
                    'checkoutState' => '000',
                    'merchantId' => $this->apiContext->getCredentials()->getMerchantId(),
                    'externalId' => 'EXT_123',
                    'amount' => '100',
                    'currency' => 'EUR',
                    'userAddress' => [
                        'name' => 'John',
                        'surname' => 'Doe',
                        'email' => 'john.doe@example.com',
                        'shippingAddress' => [
                            'receiverName' => 'John Doe',
                            'address' => 'Via del Corso',
                            'streetNumber' => '11',
                            'city' => 'Roma',
                            'cap' => '00100',
                            'district' => 'RM',
                            'country' => 'Italia',
                            'sendAt' => 'Dummy Business Center',
                            'phoneNumber' => '+393281234567'
                        ],
                        'billingAddress' => [
                            'receiverName' => 'John Doe',
                            'address' => 'Via Veneto',
                            'streetNumber' => '15',
                            'city' => 'Roma',
                            'cap' => '00100',
                            'district' => 'RM',
                            'country' => 'Italia',
                            'fiscalCode' => 'JHNDOE90R24H501K'
                        ]
                    ]
                ]
            ]
        ];

        $this->client->setMockedStatusCode(200)
            ->setMockedBody($response);

        $response = $this->client->verifyCheckout($request);

        $this->assertInstanceOf(VerifyCheckoutResponse::class, $response);
        $this->assertEquals('000', $response->status);
        $this->assertEquals('000', $response->checkoutState);
        $this->assertEquals('EXT_123', $response->externalId);
        $this->assertEquals('100', $response->amount);
        $this->assertEquals('EUR', $response->currency);

        $this->assertInstanceOf(UserAddress::class, $response->userAddress);
        $this->assertEquals('John', $response->userAddress->name);
        $this->assertEquals('Doe', $response->userAddress->surname);

        $this->assertInstanceOf(ShippingAddress::class, $response->userAddress->shippingAddress);
        $this->assertEquals('John Doe', $response->userAddress->shippingAddress->receiverName);
        $this->assertEquals('Via del Corso', $response->userAddress->shippingAddress->address);
        $this->assertEquals('11', $response->userAddress->shippingAddress->streetNumber);
        $this->assertEquals('Roma', $response->userAddress->shippingAddress->city);
        $this->assertEquals('00100', $response->userAddress->shippingAddress->cap);
        $this->assertEquals('RM', $response->userAddress->shippingAddress->district);
        $this->assertEquals('Italia', $response->userAddress->shippingAddress->country);
        $this->assertEquals('Dummy Business Center', $response->userAddress->shippingAddress->sendAt);
        $this->assertEquals('+393281234567', $response->userAddress->shippingAddress->phoneNumber);

        $this->assertInstanceOf(BillingAddress::class, $response->userAddress->billingAddress);
        $this->assertEquals('John Doe', $response->userAddress->billingAddress->receiverName);
        $this->assertEquals('Via Veneto', $response->userAddress->billingAddress->address);
        $this->assertEquals('15', $response->userAddress->billingAddress->streetNumber);
        $this->assertEquals('Roma', $response->userAddress->billingAddress->city);
        $this->assertEquals('00100', $response->userAddress->billingAddress->cap);
        $this->assertEquals('RM', $response->userAddress->billingAddress->district);
        $this->assertEquals('Italia', $response->userAddress->billingAddress->country);
        $this->assertEquals('JHNDOE90R24H501K', $response->userAddress->billingAddress->fiscalCode);

    }

    public function checkoutStatusDataProvider()
    {

        return [
            [VerifyCheckoutResponse::CHECKOUT_STATUS_COMPLETED],
            [VerifyCheckoutResponse::CHECKOUT_STATUS_PAYMENT_FAILED],
            [VerifyCheckoutResponse::CHECKOUT_STATUS_EXPIRED],
            [VerifyCheckoutResponse::CHECKOUT_STATUS_PENDING],
            [VerifyCheckoutResponse::CHECKOUT_STATUS_ALREADY_PAYED],
            [VerifyCheckoutResponse::CHECKOUT_STATUS_PREAUTHORIZED]
        ];

    }

}